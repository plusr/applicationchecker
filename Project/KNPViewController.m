//
//  KNPViewController.m
//  DialogueAd
//
//  Created by akuraru on 01/06/2015.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import "KNPViewController.h"
#import "ApplicationChecker.h"

@interface KNPViewController ()

@end

@implementation KNPViewController

- (void)viewDidLoad {
   [super viewDidLoad];

    for (ApplicationInformation *info in [ApplicationChecker inHouseApplications]) {
        NSLog(@"%@: %d", info.name, [info exist]);
    }
}
- (IBAction)open:(id)sender {
    [[ApplicationChecker ninshin] open];
}
@end
