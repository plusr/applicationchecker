//
//  KNPAppDelegate.m
//  DialogueAd
//
//  Created by CocoaPods on 01/06/2015.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import "KNPAppDelegate.h"

@implementation KNPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}
@end
