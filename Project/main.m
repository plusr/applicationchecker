//
//  main.m
//  DialogueAd
//
//  Created by akuraru on 01/06/2015.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KNPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KNPAppDelegate class]));
    }
}
