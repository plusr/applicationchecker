//
//  KNPAppDelegate.h
//  DialogueAd
//
//  Created by CocoaPods on 01/06/2015.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KNPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
