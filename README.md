# DialogueAd

[![CI Status](http://img.shields.io/travis/akuraru/DialogueAd.svg?style=flat)](https://travis-ci.org/akuraru/DialogueAd)
[![Version](https://img.shields.io/cocoapods/v/DialogueAd.svg?style=flat)](http://cocoadocs.org/docsets/DialogueAd)
[![License](https://img.shields.io/cocoapods/l/DialogueAd.svg?style=flat)](http://cocoadocs.org/docsets/DialogueAd)
[![Platform](https://img.shields.io/cocoapods/p/DialogueAd.svg?style=flat)](http://cocoadocs.org/docsets/DialogueAd)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DialogueAd is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "DialogueAd"

## Author

akuraru, akuraru@gmail.com

## License

DialogueAd is available under the MIT license. See the LICENSE file for more info.

