//
// Created by akuraru
// Copyright (c) akuraru. All rights reserved.
//

#import "ApplicationChecker.h"

@implementation ApplicationInformation {
}
- (instancetype)initWithName:(NSString *)name appleID:(NSString *)appleID URLScheme:(NSURL *)URLScheme {
    self = [super init];
    if (self) {
        _name = name;
        _appleID = appleID;
        _URLScheme = URLScheme;
    }
    return self;
}

- (BOOL)open {
    if ([self exist]) {
        return [self moveToApp];
    } else {
        return [self goToStore];
    }
}

- (BOOL)exist {
    return [[UIApplication sharedApplication] canOpenURL:[self URLScheme]];
}

- (BOOL)moveToApp {
    return [[UIApplication sharedApplication] openURL:[self URLScheme]];
}

- (BOOL)goToStore {
    NSString *string = [@"itms-apps://itunes.apple.com/app/id" stringByAppendingString:self.appleID];
    return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
}
@end

@implementation ApplicationChecker {
}

+ (NSArray *)checkApplications {
    return [[self inHouseApplications] arrayByAddingObjectsFromArray:[self competitiveApplications]];
}

// 自社アプリ
+ (NSArray *)inHouseApplications {
    return @[
             [self ninshin],
             [self ContractionsTimer],
             [self LactationBook],
             [self StepBabyFood],
             [self Cafe],

             [self FallSleepBaby],
             [self FallSleep],
             [self MaybePregnancy],
             [self NinshinMoney],
             [self jyunbilist],
             [self echoFrame],
             [self weightNote],
             [self LoveChildcare],
             [self lunanote_universal],
             [self Cradle],

             [self HBPNote],
             [self MedicineNote],
             [self HospitalNote],
             
             [self vaccine],
    ];
}

+ (ApplicationInformation *)ninshin {
    return [[ApplicationInformation alloc] initWithName:@"ママびより" appleID:@"480104468" URLScheme:[NSURL URLWithString:@"jp.co.plusr.ninshin://"]];
}

+ (ApplicationInformation *)ContractionsTimer {
    return [[ApplicationInformation alloc] initWithName:@"陣痛きたかも" appleID:@"672093792" URLScheme:[NSURL URLWithString:@"jp.co.plusr.ContractionsTimer://"]];
}

+ (ApplicationInformation *)LactationBook {
    return [[ApplicationInformation alloc] initWithName:@"授乳ノート" appleID:@"898192553" URLScheme:[NSURL URLWithString:@"jp.co.plusr.LactationBook://"]];
}

+ (ApplicationInformation *)StepBabyFood {
    return [[ApplicationInformation alloc] initWithName:@"ステップ離乳食" appleID:@"1055947417" URLScheme:[NSURL URLWithString:@"jp.co.plusr.StepBabyFood://"]];
}

+ (ApplicationInformation *)Cafe {
    return [[ApplicationInformation alloc] initWithName:@"ママびよりカフェ" appleID:@"1437608510" URLScheme: [NSURL URLWithString:@"jp.karadanote.Cafe://"]];
}

+ (ApplicationInformation *)FallSleepBaby {
    return [[ApplicationInformation alloc] initWithName:@"ぐっすリンベビー" appleID:@"1055937784" URLScheme:[NSURL URLWithString:@"jp.co.plusr.FallSleepBaby://"]];
}

+ (ApplicationInformation *)FallSleep {
    return [[ApplicationInformation alloc] initWithName:@"ぐっすりん" appleID:@"926161341" URLScheme:[NSURL URLWithString:@"jp.co.plusr.FallSleep://"]];
}

+ (ApplicationInformation *)MaybePregnancy {
    return [[ApplicationInformation alloc] initWithName:@"妊娠したかも" appleID:@"909202750" URLScheme:[NSURL URLWithString:@"jp.co.plusr.MaybePregnancy://"]];
}

+ (ApplicationInformation *)NinshinMoney {
    return [[ApplicationInformation alloc] initWithName:@"妊娠なうマネー" appleID:@"910035672" URLScheme:[NSURL URLWithString:@"jp.co.plusr.NinshinMoney://"]];
}

+ (ApplicationInformation *)jyunbilist {
    return [[ApplicationInformation alloc] initWithName:@"出産じゅんびリスト" appleID:@"1068383217" URLScheme:[NSURL URLWithString:@"jp.co.plusr.jyunbilist://"]];
}

+ (ApplicationInformation *)echoFrame {
    return [[ApplicationInformation alloc] initWithName:@"エコーフレーム" appleID:@"978597726" URLScheme:[NSURL URLWithString:@"jp.co.plusr.echoFrame://"]];
}

+ (ApplicationInformation *)weightNote {
    return [[ApplicationInformation alloc] initWithName:@"にんぷ体重" appleID:@"1031413884" URLScheme:[NSURL URLWithString:@"jp.co.plusr.WeightNote://"]];
}

+ (ApplicationInformation *)LoveChildcare {
    return [[ApplicationInformation alloc] initWithName:@"ラブ育ノート" appleID:@"664936569" URLScheme:[NSURL URLWithString:@"jp.co.plusr.LoveChildcare://"]];
}

+ (ApplicationInformation *)lunanote_universal {
    return [[ApplicationInformation alloc] initWithName:@"ルナノート" appleID:@"623062893" URLScheme:[NSURL URLWithString:@"jp.co.plusr.lunanote-universal://"]];
}

+ (ApplicationInformation *)Cradle {
    return [[ApplicationInformation alloc] initWithName:@"ぐずピタ" appleID:@"914779376" URLScheme:[NSURL URLWithString:@"jp.co.plusr.Cradle://"]];
}

+ (ApplicationInformation *)HBPNote {
    return [[ApplicationInformation alloc] initWithName:@"血圧ノート" appleID:@"629868910" URLScheme:[NSURL URLWithString:@"jp.co.plusr.HBPNote://"]];
}

+ (ApplicationInformation *)HospitalNote {
    return [[ApplicationInformation alloc] initWithName:@"通院ノート" appleID:@"649744494" URLScheme:[NSURL URLWithString:@"jp.co.plusr.HospitalNote://"]];
}

+ (ApplicationInformation *)MedicineNote {
    return [[ApplicationInformation alloc] initWithName:@"お薬ノート" appleID:@"508677361" URLScheme:[NSURL URLWithString:@"jp.co.plusr.MedicineNote://"]];
}

+ (ApplicationInformation *)vaccine {
    return [[ApplicationInformation alloc] initWithName:@"ラブベビ手帳" appleID:@"591705586" URLScheme:[NSURL URLWithString:@"jp.co.plusr.vaccine://"]];
}

// 競合アプリ
+ (NSArray *)competitiveApplications {
    return @[
             [self medart_studios_Pregnancy],
             [self sbncontractionlogger],
             [self karadaGraph],
    ];
}

+ (ApplicationInformation *)medart_studios_Pregnancy {
    return [[ApplicationInformation alloc] initWithName:@"妊娠・Sprout" appleID:@"369577475" URLScheme:[NSURL URLWithString:@"fb151534964887312://"]];
}

+ (ApplicationInformation *)sbncontractionlogger {
    return [[ApplicationInformation alloc] initWithName:@"陣痛時計" appleID:@"415881849" URLScheme:[NSURL URLWithString:@"sbncontractionlogger://"]];
}

+ (ApplicationInformation *)karadaGraph {
    return [[ApplicationInformation alloc] initWithName:@"からだグラフ" appleID:@"814321385" URLScheme:[NSURL URLWithString:@"graphi://"]];
}

@end
