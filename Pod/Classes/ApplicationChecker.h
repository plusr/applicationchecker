//
// Created by akuraru
// Copyright (c) akuraru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationInformation: NSObject
@property(readonly, nonatomic) NSString *name;
@property(readonly, nonatomic) NSString *appleID;
@property(readonly, nonatomic) NSURL *URLScheme;

- (BOOL)open;
- (BOOL)exist;
- (BOOL)moveToApp;
- (BOOL)goToStore;
@end

@interface ApplicationChecker : NSObject

// 自社アプリ一覧
+ (NSArray *)inHouseApplications;

// 自社アプリ
+ (ApplicationInformation *)ninshin; // ママびより
+ (ApplicationInformation *)ContractionsTimer; // 陣痛きたかも
+ (ApplicationInformation *)LactationBook; // 授乳ノート
+ (ApplicationInformation *)StepBabyFood; // ステップ離乳食
+ (ApplicationInformation *)Cafe; // ママびよりカフェ

+ (ApplicationInformation *)FallSleepBaby; // ぐっすリンベビー
+ (ApplicationInformation *)FallSleep; // ぐっすりん
+ (ApplicationInformation *)MaybePregnancy; // 妊娠したかも
+ (ApplicationInformation *)NinshinMoney; // 妊娠なうマネー
+ (ApplicationInformation *)jyunbilist; // 出産じゅんびリスト
+ (ApplicationInformation *)echoFrame; // エコーフレーム
+ (ApplicationInformation *)weightNote; // にんぷ体重
+ (ApplicationInformation *)LoveChildcare; // ラブ育ノート
+ (ApplicationInformation *)lunanote_universal; // ルナノート
+ (ApplicationInformation *)Cradle; // ぐずピタ

+ (ApplicationInformation *)HBPNote; // 血圧ノート
+ (ApplicationInformation *)MedicineNote; // お薬ノート
+ (ApplicationInformation *)HospitalNote; // 通院ノート

+ (ApplicationInformation *)vaccine; // ラブベビ手帳

// 競合アプリ一覧
+ (NSArray *)competitiveApplications;
+ (ApplicationInformation *)karadaGraph;

@end
