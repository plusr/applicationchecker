//
//  DialogueAdTests.m
//  DialogueAdTests
//
//  Created by akuraru on 01/06/2015.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

@implementation NSString (the)
- (NSString *)stringPath {
   NSString *s = self.lastPathComponent;
   return [self substringToIndex:self.length - s.length];
}
@end

SPEC_BEGIN(InitialTests)
describe(@"My initial tests", ^{
   for (NSString *s in @[@"stringPath"]) {
      it(s, ^{
         NSString *source = @"http://karadanote.jp/app/FullPageAdvertisement/advertisement.php";
         NSString *result= @"http://karadanote.jp/app/FullPageAdvertisement/";
         [[[source performSelector:NSSelectorFromString(s)] should] equal:result];
      });
   }
});
SPEC_END
